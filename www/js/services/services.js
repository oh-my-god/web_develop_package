angular.module('services', ['ionic'])
.factory('$r', function($http, $interface, $rootScope, _, $cache, $jf, $q) {
  return function(api, reqbody){
    // merge with cordova application
    // secure msg
    //  'reqContent={"reqHead":{"transDate":"20150101","transTime":"120000","application":"login","serialID":"222222","osType":"andriod2.4","version":"1.0","phone":"18988888888"},"reqBean":{"detail":[{"userName":"admin","password":"DFAF016D15CB38C487B5DB9E0ADA1FCC8A8959E0"}]}}&sign=E9CB490CAF0FCB2E2682409B483EC71A'
    // msg = {
    //   reqContent: '{"reqHead":{"transDate":"20150101","transTime":"120000","application":"login","serialID":"222222","osType":"andriod2.4","version":"1.0","phone":"18988888888"},"reqBean":{"detail":[{"userName":"admin","password":"DFAF016D15CB38C487B5DB9E0ADA1FCC8A8959E0"}]}}',
    //   sign: 'E9CB490CAF0FCB2E2682409B483EC71A'
    // };
    
    var reqhead = {
      "reqHead": {
        "application": api,
        "token": $cache.getToken(),
        "phone": $cache.getPhone()
      }
    };
    var reqbean = {
      "reqBean": {
        "detail":  reqbody
      }
    };
    var msg = _.merge(reqhead, reqbean);
    console.log(msg);
    var q = $q.defer(); 
    $jf.getEnv().then(function(data){
      // q.resolve(data);
      // q.resolve(_.merge(data, reqhead.reqHead));
      msg.reqHead = _.merge(data, reqhead.reqHead);
      console.log(msg);
      var reqContent = encodeURI(JSON.stringify(msg));
      $jf.digestWithMD5(reqContent).then(function(sign){
      var reqData = 'reqContent='+ encodeURI(JSON.stringify(msg)) + "&sign=" + sign;
        var req = {
            method: 'POST',
            url: $interface.getUrl(api),
            dataType:'jsonp',
            headers: {
                'Content-Type': "application/x-www-form-urlencoded",
            },
            data: reqData
            };
            $http(req).error(function(err){
                    console.log(err);
//                    $rootScope.$broadcast("network:error", err);
                    $rootScope.$broadcast("token:invalidation", data);
                    q.reject(err);
                }).success(function(data){
                    // q.resolve(data);
                    // console.log(JSON.stringify(data));
                     // token invalidation
                     if(data.respHead.code === 'FP02'||data.respHead.code === 'FP91'){
//                        $rootScope.$broadcast("token:invalidation", data);
                       q.reject(data);
                     }else {
                       q.resolve(data);
                     }
                });
        
      });
    }, function(err){
      q.reject(err); 
    });
    return q.promise;

  };
})
.factory('$rs', function($http, $interface, $rootScope, _, $cache, $jf, $q) {
  return function(api, reqbody){
    var reqhead = {
      "reqHead": {
        "application": api,
        "token": $cache.getToken(),
        "phone": $cache.getPhone()
      }
    };
   var reqbean = {
      "reqBean": {
        "detail":  reqbody
      }
    };
    var msg = _.merge(reqhead, reqbean);
    console.log(msg);
    var q = $q.defer(); 
    $jf.getEnv().then(function(data){
      // q.resolve(data);
      // q.resolve(_.merge(data, reqhead.reqHead));
      msg.reqHead = _.merge(data, reqhead.reqHead);
      console.log(msg);
      var reqData = 'reqContent='+encodeURI(JSON.stringify(msg));
      var req = {
            method: 'POST',
            url: $interface.getUrl(api),
            headers: {
                'Content-Type': "application/x-www-form-urlencoded",
            },
            data: reqData
            };
            $http(req).error(function(err){
                    console.log(err);
//                    $rootScope.$broadcast("network:error", err);
                    q.reject(err);
                }).success(function(data){       
                       q.resolve(data);
                });

    }, function(err){
      q.reject(err); 
    });
    return q.promise;
  };
})
.factory('$interface', function(API_URL){
  return {
    getUrl: function(api){
      return API_URL + api;
    }
  };
})

.directive('jfpalSelect', 
           ['$ionicModal', function($ionicModal) {
                             return {
                               
                               /* Only use as <jfpal-select> tag */
                               restrict : 'E',

                               /* Our template */
                               templateUrl: 'jfpal-select.html',

                               /* Attributes to set */
                               scope: {
                                 'items'        : '=', /* Items list is mandatory */
                                 'text'         : '=', /* Displayed text is mandatory */
                                 'value'        : '=', /* Selected value binding is mandatory */
                                 'callback'     : '&'
                               },

                               link: function (scope, element, attrs) {

                                 /* Default values */
                                 scope.multiSelect  = attrs.multiSelect === 'true' ? true : false;
                                 scope.allowEmpty   = attrs.allowEmpty === 'false' ? false : true;

                                 /* Header used in ion-header-bar */
                                 scope.headerText   = attrs.headerText || '';

                                 /* Text displayed on label */
                                 // scope.text      = attrs.text || '';
                                 scope.defaultText  = scope.text || '';

                                 /* Notes in the right side of the label */
                                 scope.noteText     = attrs.noteText || '';
                                 scope.noteImg      = attrs.noteImg || '';
                                 scope.noteImgClass = attrs.noteImgClass || '';

                                 
                                 /* Optionnal callback function */
                                 scope.callback     = attrs.callback || null;

                                 /* Instanciate ionic modal view and set params */

                                 /* Some additionnal notes here : 
                                  * 
                                  * In previous version of the directive,
                                  * we were using attrs.parentSelector
                                  * to open the modal box within a selector. 
                                  * 
                                  * This is handy in particular when opening
                                  * the "fancy select" from the right pane of
                                  * a side view. 
                                  * 
                                  * But the problem is that I had to edit ionic.bundle.js
                                  * and the modal component each time ionic team
                                  * make an update of the FW.
                                  * 
                                  * Also, seems that animations do not work 
                                  * anymore.
                                  * 
                                  */
                                 $ionicModal.fromTemplateUrl(
                                   'jfpal-select-items.html',
                                   {'scope': scope}
                                 ).then(function(modal) {
                                   scope.modal = modal;
                                 });

                                 /* Validate selection from header bar */
                                 scope.validate = function (event) {
                                   // Construct selected values and selected text
                                   if (scope.multiSelect == true) {

                                     // Clear values
                                     scope.value = '';
                                     scope.text = '';

                                     // Loop on items
                                     angular.element.each(scope.items, function (index, item) {
                                       if (item.checked) {
                                         scope.value = scope.value + item.id+';';
                                         scope.text = scope.text + item.text+', ';
                                       }
                                     });

                                     // Remove trailing comma
                                     scope.value = scope.value.substr(0,scope.value.length - 1);
                                     scope.text = scope.text.substr(0,scope.text.length - 2);
                                   }

                        // Select first value if not nullable
                        if (typeof scope.value == 'undefined' || scope.value == '' || scope.value == null ) {
                            if (scope.allowEmpty == false) {
                                scope.value = scope.items[0].id;
                                scope.text = scope.items[0].text;

                                // Check for multi select
                                scope.items[0].checked = true;
                            } else {
                                scope.text = scope.defaultText;
                            }
                        }

                        // Hide modal
                        scope.hideItems();
                        
                        // Execute callback function
                        if (typeof scope.callback === 'function') {
                            scope.callback(scope.value);
                        } else if (typeof scope.callback === 'string'){
                            scope.$parent[scope.callback].call(scope.$parent, scope.item);
                        }
                    };

                    scope.confirmWithOutValidate = function (event) {
                        if (typeof scope.callback === 'string'){
                            scope.$parent[scope.callback].call(scope.$parent, scope.items);
                        }
                    };

                    /* Show list */
                    scope.showItems = function (event) {
                        event.preventDefault();
                        scope.modal.show();
                    };

                    /* Hide list */
                    scope.hideItems = function () {
                        scope.modal.hide();
                    };

                    /* Destroy modal */
                    scope.$on('$destroy', function() {
                        scope.modal && scope.modal.remove();
                    });

                    // scope.$on("selected", function(){
                    //   angular.element.each(scope.items, function (index, item) {
                    //      item.selected = false;
                    //   });
                    // });

                    /* Validate single with data */
                    scope.validateSingle = function (item) {
                      
                        scope.item = item;

                        // Set selected text
                        scope.text = item.text;

                        // Set selected value
                        scope.value = item.id;

                        scope.$emit("selected");
                        item.selected = true;

                        // Hide items
                        // scope.hideItems();
                        
                        // Execute callback function
                        if (typeof scope.callback == 'function') {
                            scope.callback(scope.value);
                        }
                        // invoke onselect
                        scope.validate();
                      
                    };
                }
            };
        }
    ]
)
.directive('jfpalSubview', ['$ionicModal',
function($ionicModal) {
    return {

        /* Only use as <jfpal-select> tag */
        restrict : 'E',

        /* Our template */
        templateUrl: 'jfpal-subview.html',

        /* Attributes to set */
        scope: {
            'obj'          : '=',
            'callback'     : '&',
            'before'       : '&'
        },

        link: function (scope, element, attrs) {

            $ionicModal.fromTemplateUrl(
                'jfpal-subview-detail.html', {'scope': scope}
            ).then(function(modal) {
                scope.modal = modal;
            });

            scope.validate = function (event) {
                // Hide modal
                scope.hideItems();
              
                // if (typeof scope.callback === 'string'){
                //     scope.$parent[scope.callback].call(scope.$parent, scope.obj);
                // }else{
                //    console.error("callback should be a function name");
                // }
                scope.callback({
                  $obj: scope.obj
                });
            };
          
            scope.$on('$destroy', function() {
                scope.modal && scope.modal.remove();
            });

            /* Show list */
            scope.showItems = function (event) {
                event.preventDefault();
                if(typeof scope.before === 'function'){
                    if(scope.before({$obj: scope.obj})){
                        scope.modal.show();
                    }
                }else{
                    scope.modal.show();
                }
            };

            /* Hide list */
            scope.hideItems = function () {
                scope.modal.hide();
            };
        }
    };
}]
)
.service("validation", function(){
    this.isnull=function(text){
      if(text == undefined || text == null || text == "")
      {
        return false;
      }else{
        return true;
      }
      // return (text === undefined || text == null || text === "");
    };
    this.money=function(money){
      return  (/^\d+(\.\d+)?$/.test(money));
    };
    this.phone =function(mobile){
        return (/^1[3|4|5|7|8][0-9]\d{8}$/.test(mobile));
    };
    this.number = function(number){
        return(/^[1-9]+\d*$/.test(number));
    };
    this.cardid = function(cardid){
      return(/^(\d{16}|\d{19})$/.test(cardid));
    };
})

.service('$h', function($state, $ionicHistory) {

  this.getHistory = function(){
    return $ionicHistory.viewHistory();
  }

  this.setBackToState = function(state){
    var histack = $ionicHistory.viewHistory().histories[name? name: "root"].stack,
        views = $ionicHistory.viewHistory().views,
        viewId,
        the_idx;

    histack.forEach(function(view, idx) {
      if(view.stateName == state) {
        viewId = view.viewId
        the_idx  = idx;
        return;
      }
    });
    if(viewId){
      $ionicHistory.viewHistory().backView = views[viewId]
      for(var i = histack.length; i > the_idx ; i--){
        delete histack[i];
      }
    }
    return viewId;
  };

  this.popToState = function(state){
    
    this.setBackToState(state);
    $ionicHistory.goBack();
  };

    this.gotoapp=function(){
      $state.go("app");
      $ionicHistory.nextViewOptions({
         disableAnimate: true,
         //disableBack: true
         historyRoot: true,
         expire: 100
      });
  };
})

.factory("$cache", function(){
  var user = {
      token: "0000",
      username:"",
      phone: "",
      idcard: "",
      personimg:""
  },
  withdraw_bank= {};
  return {
    resetToken: function(){
      user.token = "0000"; 
    },
    setUser:function(user_o){
      user=user_o;
    },
    getPhone: function(){
     return user.phone; 
    },
    getToken: function(){
      return user.token;
    },
    setToken: function(value){
      user.token = value;
      // todo store token to app
    },
    setPhone: function(phone){
      user.phone = phone;
    },
    getBank: function(key){
        return withdraw_bank[key];
    },
    setBank: function(key,bank){
        withdraw_bank[key] = bank;
    },
    getIdCard: function(){
      return user.idcard;
    },
    setIdCard: function(idcard){
      user.idcard=idcard;
    },
    setUserName:function(name){
        user.username=name;
    },
    getUserName:function(){
        return user.username;
    },
    setPersonimg:function(personimg){
        user.personimg=personimg;
    },
    getPersonimg:function(){
        return user.personimg;
    }
  };
})
.filter('bank_url', function() {
  return function(url) {
    return "../www/img/bank_icons/bank_" + url+"@2x.png";
  };
})
.filter('img_url', function() {
  return function(url) {
    return "./img/public/" + url+".png";
  };
})
.filter('produ_url', function() {
  return function(url) {
    return "./img/product/" + url+".png";
  };
})
.filter('photo_url', function() {
  return function(url) {
    return "http://192.168.50.89:2016/getphoto/"+ url;
  };
})
.filter('tomoney', function() {
    return function(s, n) { 
          n = n > 0 && n <= 20 ? n : 2;
          var money = parseFloat(s);
          if(money){
            s = ( money / 100 ).toFixed(n) + "";
            var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1];
            t = "";
            for (i = 0; i < l.length; i++) {
              t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
            }
            return t.split("").reverse().join("") + "." + r;
          }else if (s == 0 ){
            return "0.00"
          } else {
            return '---';

          }
    };
})
.filter('todate', function() {
    return function(thisdate) { 
      if (thisdate) {
        var y=thisdate.substr(0,4);
        var m=thisdate.substr(4,2);
        var d=thisdate.substr(6,2);
        return y+'-'+m+'-'+d;
      }else{
        return  '---';
      };
    };
})
.filter('totime',function(){
  return function(thistime){
    if (thistime) {
      var h=thistime.substr(0,2);
      var m=thistime.substr(2,2);
      return h+':'+m;
    }else{
      return '---';
    };
  };
})
.filter('stringtotime',function(){
  Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
  };
  return function(thistime){
    return new Date(thistime).Format("MM-dd hh:mm");  
  };
  
})

;
