angular.module('controllers')
.controller('ProductsCtrl',function($scope,$r, $q,$rs){
// productslist
  var load=function() {
       var product_list ={};
        $rs("products", product_list ).then(function(data){
          $scope.products=data.docs;
          $scope.$broadcast('scroll.refreshComplete');
          console.log(data);
          }, function(err){
          console.log(err);
        });
  };
  $scope.$on('$ionicView.enter', function() {
             console.log("$ionicView.enter");
        load();
  });
  $scope.doRefresh=function(){
       load();
  };
})
.controller('TabScriptCtrl', function($scope, $element,$ionicGesture) {
	  $scope.form={};
    $scope.form.clickstatus={};
    $scope.onDragUp=function(obg){
    console.log(obg);
    if($scope.form.clickstatus[obg]==true){
      $scope.form.clickstatus[obg]=false;
    }else{
      $scope.form.clickstatus[obg]=true;  
    }
    console.log($scope.form.clickstatus);
    };
//    var targ=obg.target.parentNode.getElementsByClassName('absolute_bottom');
//    $ionicGesture.on('dragup', function(e) {
//           //call the controller method
//           console.log(e);
//     },$element);
})
.controller('ProductlistCtrl',function($scope,$rs, $q){
// productslist
  $scope.$on('$ionicView.enter', function() {
             console.log("$ionicView.enter");
          var product_list = {};
        $rs("products", product_list ).then(function(data){
          $scope.products=data.docs;
          console.log(data);
          }, function(err){
          console.log(err);
        });
  });
})
.controller('ProductsDetailCtrl',function($scope,$rs,$stateParams,$state, $cache, toaster, validation ,$ionicActionSheet ,$ionicModal,$ionicSlideBoxDelegate){
      $scope.form={};
      $scope.form.productQuantity='';
      $scope.form.submits=true;
      $scope.form.agree=true;
      $scope.$on('$ionicView.enter', function() {
            var product_detail={
                "id":$stateParams.productCode
                };
            $rs("productDetail", product_detail ).then(function(data){
                  $scope.productsdetail=data.docs[0];
                  $ionicSlideBoxDelegate.update();
                  $ionicSlideBoxDelegate.start();
                  console.log(data);
              },function(err){
                  console.log(err);
             }); 
             console.log("$ionicView.enter");
      });

      $scope.rulesShow = function(){
         $scope.rules.show();
      };
       $scope.closemodal = function() {
          $scope.rules.hide();
      };


     $ionicModal.fromTemplateUrl('templates/rules.html', {
            scope: $scope
          }).then(function(rules) {
            $scope.rules = rules;
    });
});