angular.module('starter')
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })
//tabs
  .state('app.tabs', {
      url: "/tab",
      // abstract: true,
      views: {
      'menuContent': {
       templateUrl: "templates/tab.html",
        controller: 'ProductsCtrl'
      }
    }
   })
   .state('app.tabs.home', {
      url: "/home",
      views: {
        'home-tab': {
          templateUrl: "templates/products.html",
          controller: 'ProductsCtrl'
        }
      }
    })
//normal tpl
  .state('app.login', {
    url: "/login",
    views: {
      'menuContent': {
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl'
      }
    }
  })

  .state('app.products', {
    url: "/products",
    views: {
      'menuContent': {
        templateUrl: "templates/products.html",
        controller: 'ProductsCtrl'
      }
    }
  })

 .state('app.setting', {
    url: "/setting",
    views: {
      'menuContent': {
        templateUrl: "templates/setting.html",
      }
    }
  })
  ;
   $urlRouterProvider.otherwise('/app/tab/home');
});
