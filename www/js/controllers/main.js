angular.module('controllers', [])
.controller('AppCtrl', function($rootScope,$scope, $ionicModal, $timeout, validation, toaster, $state ,$rs, $cache , $ionicHistory,$ionicLoading) {
  $scope.form={};
  $scope.form.username=$cache.getUserName();
  $scope.form.personimg=$cache.getPersonimg();
  $scope.form.num=0;
  $scope.$on("token:invalidation", function(){
    $ionicHistory.clearHistory();
    $ionicHistory.nextViewOptions({
               disableAnimate: true,
               historyRoot: true
    });
    var msg="登录信息过期";
    toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
    $state.go("app.login");
  });
  $rootScope.$on("token:refresh", function(){
       $scope.form.username=$cache.getUserName();
       $scope.form.personimg=$cache.getPersonimg();
  });
  
  $scope.domenumove=function(){
    $scope.form.num+=1;
    if($scope.form.num%2==0){
      $scope.form.moveas="";
    }else{
      $scope.form.moveas="moveas";  
    }
    
  };

})

.controller('LoginCtrl', function($rootScope,$scope,$ionicModal, $timeout, validation, toaster, $state ,$rs, $cache , $ionicHistory,$ionicLoading) {
    $scope.form={};
    $scope.form.usernames=$cache.getUserName();
    console.log($scope.form.usernames);
    $scope.form.submits=true;
    $scope.form.subtext="登录";
    var msg;
    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
           console.log(validation.phone($scope.form.phone));
          if(validation.isnull($scope.form.phone)){
            if(validation.phone($scope.form.phone)){
                   if(validation.isnull($scope.form.password)){
                    $scope.form.submits=false;
                    $scope.form.subtext="登录中...";
                      var loginform = {
                        'phone':$scope.form.phone,
                        'password':$scope.form.password
                      };
                      $cache.setPhone($scope.form.phone);
                      $rs("login", loginform).then(function(data){
                        console.log(data);
                         if(data.code=="200"){
                           var user={
                                    'token':'admins',
                                    'username':data.docs[0].username,
                                    'idcard':data.docs[0]._id,
                                    'phone': data.docs[0].phone
                                  };
                                  $cache.setUser(user);
                                  $cache.setUserName(data.docs[0].username);
                                  $cache.setIdCard(data.docs[0]._id);
                                  if(data.docs[0].personfile){
                                    $cache.setPersonimg(data.docs[0].personfile.personimg);
                                  }
                                  
                                  $scope.form.username=$cache.getUserName();
                                  console.log($scope.form.username);
                                  $scope.form.submits=true;
                                  $scope.form.subtext="登录";
                                  $rootScope.$broadcast("token:refresh", data);
                                  $state.go("app.tabs");
                                 
                                  $ionicHistory.nextViewOptions({
                                     disableAnimate: true,
//                                     disableBack: true
                                     historyRoot: true
                                  });

                            }else{
                              $scope.form.submits=true;
                              $scope.form.subtext="登录";
                              var msg=data.message;
                              return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml'); 
                            }
                        // $scope.closeLogin();
                        },function(err){
                          $scope.form.submits=true;
                          $scope.form.subtext="登录";
                          console.log(err);
                          var msg=err.message;
                          return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                      }); 
                      
                    }else{
                        msg = "密码不能为空";
                        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
                    }
              }else{
                        msg = "请填写正确的手机号";
                        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
              }
          }else{
                        msg = "手机号不能为空";
                        return  toaster.pop('warning', null, '<ul><li>'+  msg + '</li></ul>', null, 'trustedHtml');
          }
 
    };
})
;
