
var exec = require('cordova/exec'),
    cordova = require('cordova');

module.exports = {
    getEnv: function(data, success, fail) {
        exec(success, fail, "CordovaJFService", "getEnv", [data]);
    },

    RSAEncode: function(data, success, fail) {
        exec(success, fail, "CordovaJFService", "RSAEncode", [data]);
    },

    MD5Encode: function(data, success, fail) {
        exec(success, fail, "CordovaJFService", "MD5Encode", [data]);
    },

    getPerference: function(data, success, fail) {
        exec(success, fail, "CordovaJFService", "getPerference", [data]);
    },
    
    setPerference: function(data, success, fail) {
        exec(success, fail, "CordovaJFService", "setPerference", [data]);
    }
};
