angular.module('pipe.services', ['ionic'])
.factory('$r_xml', function($http, $rootScope, _, $jf, $q, $pipe){
  return function (api, reqbody){
    var q = $q.defer();
    // 拼请求头  env + token + api
    // 拿token信息
    $jf.getAppInfo().then(function (data){
      // data.header
      // data.body
      console.log(data)
      var reqhead = {
        "reqHead": {
          "application": api,
        }
      };
      reqhead.reqHead = _.merge(reqhead.reqHead, data.header);
      reqbody = _.merge(reqbody, data.body)
      // 拼请求体
      var reqbean = {
        "reqBean": reqbody
      };
      var msg = _.merge(reqhead, reqbean);
      console.log(msg)
      var xmlRequst = $pipe.request(msg)
      console.log(xmlRequst);
      // sign change true
      $jf.getSign(xmlRequst).then(function (reqData){
        // req xml string
        var req = {
          method: 'POST',
          url: 'https://mfront.jfpal.com:443/unifiedAction.do',//生产
          // url: 'https://mfront.jfpal.com:5557/unifiedAction.do',//准生成
          headers: {
              'Content-Type': "application/x-www-form-urlencoded",
          },
          data: reqData
        };
        console.log(reqData)
        $http(req).error(function(err){
              console.log(err);
              $rootScope.$broadcast("network:error", err);
              q.reject(err);
            }).success(function(data){
              console.log("success====>" + data + "success")
               var data = $pipe.response(data)
            });
      }, function(err){
        q.reject(err);
      })
    }, function(err){
      q.reject(err);
    });
    return q.promise;
  }
})
.factory('$format', function (){
  return {
    xml2json: function(str){
      var arr = str.split('\<\/');
      var arr_new = [];
      for(i = 0; i <arr.length - 1; i++){
        var keyname = arr[i].replace(/(.*)<(.*)>(.*)/g,"$2");
        var m = arr[i].indexOf(keyname) + keyname.length + 1;
        var n = arr[i].length;
        var keyvalue = arr[i].substr(m,n);
        arr_new[keyname] = keyvalue;
      }
      var json = {};
      for (var key in arr_new) {
        json[key] = arr_new[key]
      }
      return json
    }
  };
})
.factory('$pipe', function ($format, _){
  return {
    request: function(json){
      var reqHead_str = '';
      var request_str = '';
      for(var keyname in json.reqHead) {
        var val = json.reqHead[keyname];
        reqHead_str = reqHead_str + ' ' +keyname + '="' + val + '"'
      }
      reqHead_str = '<JFPay' + reqHead_str +'>'
      if (json.reqBean != undefined && typeof(json.reqBean)==='object' && json.reqBean) {
        var hasProp = false;
        for (var prop in json.reqBean){
          hasProp = true;
          break;
        }  
        if (hasProp){
          var reqBean_str = '';
          for(var keyname in json.reqBean) {
            var val = json.reqBean[keyname];
            reqBean_str = reqBean_str + '<' +keyname + '>' + val + '</' +keyname + '>'
          }
          request_str = reqHead_str + reqBean_str;
        }else{
          request_str =  reqHead_str;
        }
        return request_str + '</JFPay>'
      }
    },
    response: function(str){
      // xml
      var m = str.indexOf('<JFPay');
      var str = str.substr(m, str.length-m);
      var xml_head = str.substr(0, str.indexOf('>')+1)
      var xml_log = str.substr(str.indexOf('>')+1, str.length -(str.indexOf('>')+1+8))//去掉</JFPal>的长度
      var n1 = xml_log.indexOf("<data>");
      var n2 = xml_log.indexOf("</data>")+7;
      xml_log = xml_log.substr(0, n1) + xml_log.substr(n2, xml_log.length-n2)
      var xml_data = str.replace(/(.+)\[CDATA\[(.+)\]\]><\/data>(.+)/g,"$2")
      // json
      var json_head = xml_head.replace(/<JFPay (.+)>/g,"$1").replace(/\s+/g,',').replace(/\=/g,":")
      json_head = eval("(" + "{"+json_head+"}" + ")")
      var json_log = $format.xml2json(xml_log)
      var json_body = eval("(" +xml_data + ")")
      // resp
      var respBean = _.merge(json_log, json_body)
      var response = {
        "respHead": json_head,
        "respBean": respBean
      }
      console.log(response)
      return response
    }
  };
})
;
