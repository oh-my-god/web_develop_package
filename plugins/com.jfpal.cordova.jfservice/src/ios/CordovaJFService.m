//
//  CordovaJFService.h
//  CordovaJFService
//
//  Created by Wangjing on 15/4/13.
//  Copyright (c) 2015年 jfpal. All rights reserved.
//

#import <Cordova/CDV.h>
#import "CordovaJFService.h"
#import "Common.h"
#import "PyEncode.h"

#ifndef UserDefaults
#define UserDefaults [NSUserDefaults standardUserDefaults]
#endif


@interface CordovaJFService ()
@end

@implementation CordovaJFService

- (void)getEnv:(CDVInvokedUrlCommand*)command
{
    NSLog(@"%s\n%@", __func__, command.arguments);

    NSMutableDictionary  *dic = [Common getCommonRequestParams];
    [dic setObject:[Common getTransLog] forKey:@"transLog"];

    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dic];
    [self.commandDelegate runInBackground:^{
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)RSAEncode:(CDVInvokedUrlCommand *)command
{
    NSLog(@"%s\n%@", __func__, command.arguments);
    id originInfo = [command.arguments firstObject];

    CDVPluginResult *pluginResult = nil;
    if(nil == originInfo || ![originInfo isKindOfClass:NSClassFromString(@"NSDictionary")] || nil == originInfo[@"account"] || nil == originInfo[@"password"])
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"参数异常"];
    }
    else
    {
        NSString *tmpStr = [NSString stringWithFormat:@"%@%02lu%@%@%@%@",
                            @"00000000",
                            (unsigned long)[originInfo[@"password"] length],
                            originInfo[@"password"],
                            [[Common getCommonRequestParams] objectForKey:@"transDate"],
                            [[Common getCommonRequestParams] objectForKey:@"transTime"],
                            (nil ==originInfo[@"account"] || [(NSString *)originInfo[@"account"] isEqualToString:@""]) ? @"0000" : (NSString *)originInfo[@"account"]
                            ];
        NSString *encodeStr = [PyEncode EnCodeRegistPassword:tmpStr];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:encodeStr];
    }
    [self.commandDelegate runInBackground:^{
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}


- (void)MD5Encode:(CDVInvokedUrlCommand *)command
{
    NSLog(@"%s\n%@", __func__, command.arguments);
    NSString *originStr = (NSString *)[command.arguments firstObject];

    CDVPluginResult *pluginResult = nil;
    if(nil == originStr)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"参数不能为空"];
    }
    else
    {
        NSString *encodeStr = [[[NSString stringWithFormat:@"%@%@", [originStr URLEncodedString], APP_SIGN_KEY] md5] uppercaseString];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:encodeStr];
    }
    [self.commandDelegate runInBackground:^{
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)setPerference:(CDVInvokedUrlCommand *)command
{
    NSDictionary *info = [command.arguments firstObject];

    NSMutableDictionary *stoge = [UserDefaults objectForKey:@"web_stoge"];
    if(nil == stoge)
    {
        stoge = [NSMutableDictionary dictionary];
    }
    [stoge addEntriesFromDictionary:info];
    [UserDefaults setObject:info forKey:@"web_stoge"];
    [UserDefaults synchronize];

    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"设置成功"];
    [self.commandDelegate runInBackground:^{
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)getPerference:(CDVInvokedUrlCommand *)command
{
    NSString *key = [command.arguments firstObject];

    NSMutableDictionary *stoge = [UserDefaults objectForKey:@"web_stoge"];
    NSString *rst = [stoge objectForKey:key];

    CDVPluginResult *pluginResult = nil;

    if(nil != rst)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:rst];
    }
    else
    {
        NSString *errMsg = [NSString stringWithFormat:@"%@对应的值不存在", key];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errMsg];
    }

    [self.commandDelegate runInBackground:^{
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

@end
