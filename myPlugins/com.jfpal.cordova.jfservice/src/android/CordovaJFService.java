package com.jfpal.licai.cordova;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.google.gson.JsonObject;
import com.jfpal.cordova.BaseCordovaActivity;
import com.jfpal.utils.LogUtils;

public class CordovaJFService extends CordovaPlugin {

	
	
	
	CordovaServiceManager mCordovaServiceManager;
	
	

	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		
		
		LogUtils.printInfo("CordovaJFService execute:"+args.toString());

		
		 boolean result=  mCordovaServiceManager.doService(action, args,callbackContext);
		   
		 
			
		

		return result;
	}

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		// TODO Auto-generated method stub
		super.initialize(cordova, webView);
		
		LogUtils.printInfo("CordovaJFService initialize");
		
		
		mCordovaServiceManager=new CordovaServiceManager(cordova.getActivity());
	}

}
